#include <SoftwareSerial.h>    //Include the softwareserial library, because the attiny isn't capable of Serial output otherwise 

#define LED_1 PB2             // define the two LED pins
#define LED_2 PB0
#define BUTTONPIN PB4         //define the Button Pin
#define RX PB3                //define RX and TX for the Softwareserial
#define TX PB1

SoftwareSerial s(RX,TX);    // initialize "s" as the Softwareserial with the RX ans TX pins

long timePress = 0;         //initialize the needed Variables  
long timePressLimit = 0;    //note that first two variables are "long", because the millis() function can put out really big numbers
int buttonPressed;
int presses = 0;
void setup() {
  pinMode(BUTTONPIN ,INPUT);    //defining the buttonpin as input
  pinMode(LED_1, OUTPUT);       //defining both LED pins as outputs
  pinMode(LED_2, OUTPUT);
  
}

void loop() {
  s.begin(9600);                          //set Baudrate of Softwareserial to 9600 (for some reason didn't work in setup)
  buttonPressed = digitalRead(BUTTONPIN);       //setting the "buttonpressed" variable to the state of the Buttonpin
  
  if (buttonPressed == HIGH){               //when the button is pressed, set the timePress variable to the current time since the Sketch has been loaded,
    delay(200);                             //then set the timePressLimit to 400ms more than that and increase presses to 1
    if (presses == 0) {
    timePress = millis();
    timePressLimit = timePress + 400;    
    presses = 1;
    
    }
    
    if (presses == 1 && millis() < timePressLimit){      //if presses is 1 and the current time is not yet bigger than "timePressLimit" 
      s.println("User input: Doubleclick");               //print out using the Softwareserial and make the LED_2 blink 5 times using a while loop
                                                          
      int i = 5;     
      while(i>0){
      digitalWrite(LED_2, HIGH);
      delay(300);
      digitalWrite(LED_2, LOW);
      delay(300);
      i--;
      }
      
      timePress = 0;                                    //reset all of the variables, to ba able to click again
      timePressLimit = 0;
      presses = 0; 
           
    }    
  }

  if (presses == 1 && timePressLimit != 0 && millis() > timePressLimit){        //When presses is 1, but the cuurent time is bigger than the timePressLimit,
     s.println("User input: Singleclick");                                  //the user only pressed the button once. Therefore we print a message using the Softwareserial
     
     
      for(int i = 0; i<5;i++){                          //let the LED_1 blink 5 times, but this time using a for loop, to demonstrate different kinds of loops
      digitalWrite(LED_1, HIGH);
      delay(300);
      digitalWrite(LED_1, LOW);
      delay(300);
      }                                           
      timePress = 0;                         //reset all of the variables, to ba able to click again
     timePressLimit = 0;
     presses = 0;
   }  
}
